<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- El token no es necesario porque Laravel genera uno por nosotros, pero para eliminar el error en la consola del navegador se puede escribir el siguiente token --}}
        <meta name="csrf-token" content="{!! csrf_token() !!}">

        <title>Laravel - Components VUEjs</title>
        <link rel="stylesheet" href="{!! asset('css/app.css') !!}">

    </head>
    <body>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <div id="app">
                        <idea></idea>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="{!! asset('js/app.js') !!}"></script>
    </body>
</html>
